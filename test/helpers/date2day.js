
import date2day from '../../app/helpers/date2day.js'

describe('date2day', () => {
  it('returns correct 3 character day', () => {
    const sat = new Date(2016, 8, 24)
    expect(date2day(sat)).toEqual('Sat')
  })
  it('accept string date', () => {
    expect(date2day('2016-09-24')).toEqual('Sat')
    expect(date2day('2016 September 24')).toEqual('Sat')
    expect(date2day('September 24, 2016')).toEqual('Sat')
  })
  it('accept Date', () => {
    expect(date2day(new Date(2016, 8, 24))).toEqual('Sat')
  })
  it('accept EPOCH timestamp', () => {
    expect(date2day(1474709741215)).toEqual('Sat')
  })
  it("returns 'Invalid Date' when input is invalid date", () => {
    expect(date2day('24/09/2016')).toEqual('Invalid Date')
    expect(date2day('ABCDEF')).toEqual('Invalid Date')
    expect(date2day('1474709741215')).toEqual('Invalid Date')
  })
})
