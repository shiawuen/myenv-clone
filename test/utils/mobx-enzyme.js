
import React from 'react'
import { Provider } from 'mobx-react'
import { mount as _mount } from 'enzyme'

export const mount = (component, state = {}) => {
  return _mount(
    <Provider {...state}>
      {component}
    </Provider>
  )
}

