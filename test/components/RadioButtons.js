
import React from 'react'
import { shallow, render } from 'enzyme'
import RadioButtons from '../../app/components/RadioButtons.js'

describe('RadioButtons', () => {
  it('can have RadioButton as children', () => {
    const zero = shallow(<RadioButtons name='buttons' items={[]} />)
    expect(zero.find('RadioButton').length).toEqual(0)

    const one = shallow(<RadioButtons name='buttons' items={[1]} />)
    expect(one.find('RadioButton').length).toEqual(1)

    const many = shallow(<RadioButtons name='buttons' items={[1, 2, 3]} />)
    expect(many.find('RadioButton').length).toEqual(3)
  })

  it('defaults to empty children', () => {
    const empty = shallow(<RadioButtons name='buttons' />)
    expect(empty.find('RadioButton').length).toEqual(0)
  })
})
