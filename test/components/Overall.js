
import React from 'react'
import { observable } from 'mobx'
import { mount } from '../utils/mobx-enzyme.js'
import Overall from '../../app/components/Overall.js'

describe('Overall', () => {
  it('reflect data changes to overall state', () => {
    const data = observable({ weather: 'Cloudy', temperature: 27.9, humidity: 92 })
    const component = mount(<Overall />, { overall: data })

    let text = component.text()
    expect(text.includes(data.weather))
    expect(text.includes(`${data.temperature}°C`))
    expect(text.includes(`${data.humidity}%`))

    data.weather = 'Fair'
    data.temperature = 39.5
    data.humidity = 40
    text = component.text()

    expect(text.includes(data.weather))
    expect(text.includes(`${data.temperature}°C`))
    expect(text.includes(`${data.humidity}%`))
  })
})
