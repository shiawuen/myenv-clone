
import React from 'react'
import { shallow } from 'enzyme'
import WeatherBox, { WeatherBoxItem } from '../../app/components/WeatherBox.js'

describe('WeatherBox', () => {
  it('have weather for 5 locations', () => {
    const box = shallow(<WeatherBox />)
    expect(box.find(WeatherBoxItem).length).toEqual(5)
  })
})

describe('WeatherBoxItem', () => {
  it('default location to center', () => {
    const item = shallow(<WeatherBoxItem />)
    expect(item.instance().props.location).toEqual('center')
  })
  it('default status to fair', () => {
    const item = shallow(<WeatherBoxItem />)
    expect(item.instance().props.status).toEqual('fair')
  })
})
