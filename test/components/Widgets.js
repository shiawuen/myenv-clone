
import React from 'react'
import { observable } from 'mobx'
import { mount } from '../utils/mobx-enzyme.js'
import Widgets, {
  Widget,
  WidgetDengue,
  WidgetPSI,
  WidgetAdd
} from '../../app/components/Widgets.js'

describe('Widgets', () => {
  it('contains WidgetAdd by default', () => {
    const component = mount(<Widgets />, { widgets: [] })
    expect(component.find(Widget).length).toBe(0)
    expect(component.find(WidgetDengue).length).toBe(0)
    expect(component.find(WidgetPSI).length).toBe(0)
    expect(component.find(WidgetAdd).length).toBe(1)
  })

  it('renders WidgetPSI when widget type == psi', () => {
    const component = mount(<Widgets />, { widgets: [{ type: 'psi' }] })
    expect(component.find(Widget).length).toBe(0)
    expect(component.find(WidgetDengue).length).toBe(0)
    expect(component.find(WidgetPSI).length).toBe(1)
    expect(component.find(WidgetAdd).length).toBe(1)
  })

  it('renders WidgetDengue when widget type == dengue', () => {
    const component = mount(<Widgets />, { widgets: [{ type: 'dengue' }] })
    expect(component.find(Widget).length).toBe(0)
    expect(component.find(WidgetPSI).length).toBe(0)
    expect(component.find(WidgetDengue).length).toBe(1)
    expect(component.find(WidgetAdd).length).toBe(1)
  })

  it('renders Widget when widget type != dengue or type != psi', () => {
    const component = mount(<Widgets />, { widgets: [{ type: 'anything' }] })
    expect(component.find(WidgetPSI).length).toBe(0)
    expect(component.find(WidgetDengue).length).toBe(0)
    expect(component.find(Widget).length).toBe(1)
    expect(component.find(WidgetAdd).length).toBe(1)
  })
})
