
const metadata = require('./package.json')
const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')

const is_prod = 'production' == process.env.NODE_ENV

const config = {
  devtool: is_prod ? 'source-map': 'inline-source-map',

  entry: [
    './app/index.js'
  ],

  output: {
    path: './dist',
    filename: 'index.js',
  },

  devServer: {
    hot: true,
    inline: true,
    contentBase: './dist',
    port: 8080,
    historyApiFallback: true,
  },

  module: {
    preLoaders: [
      { test: /\.js$/,
        loader: 'standard-loader',
        exclude: /(node_modules|test)/
      },
    ],
    loaders: [
      { test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      { test: /\.css$/,
        loader: 'classnames!style!css?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss'
      },
      { test: /\.(png|woff|woff2|eot|ttf|svg)$/,
        loader: 'url-loader?limit=100000'
      },
      { test: /.json$/,
        loader: 'json-loader'
      }
    ]
  },

  plugins: [
    new CopyWebpackPlugin([
      { from: 'app/index.html' }
    ]),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify(is_prod ? 'production' : 'development'),
        'ENV': JSON.stringify(is_prod ? 'production' : 'development'),
        'VERSION': JSON.stringify(metadata.version)
      }
    }),
    new webpack.optimize.UglifyJsPlugin({ compress: is_prod ? { warnings: false } : false })
  ],

  postcss: [
    require('autoprefixer')
  ],
}

module.exports = config
