
const days = {
  0: 'Sun',
  1: 'Mon',
  2: 'Tue',
  3: 'Wed',
  4: 'Thu',
  5: 'Fri',
  6: 'Sat'
}

export default (date) => {
  const d = new Date(date)
  if (d.toString() === 'Invalid Date') return d.toString()
  return days[d.getDay()]
}
