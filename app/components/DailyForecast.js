
import React from 'react'
import css from './DailyForecast.css'
import icon from './Icon.css'
import date2day from '../helpers/date2day.js'

const { string, number, arrayOf } = React.PropTypes

const DailyForecast = (data) => (
  <article className={css.wrapper}>
    <div className={css.weather}>
      <div className={css.day}>{date2day(data.date)}</div>
      <div className={icon('weather', 'blue', data.weather)} />
    </div>
    <div>{data.summary}</div>
    <footer>
      <div className={css.summary}>
        <span className={icon('temperature', 'blue', css.icon)} />
        {data.temperature.join('-')}°C
      </div>
      <div className={css.summary}>
        <span className={icon('wind', 'blue', css.icon)} />
        {data.wind.join('-')}km/h
      </div>
    </footer>
  </article>
)

DailyForecast.propTypes = {
  date: string,
  weather: string,
  temperature: arrayOf(number),
  wind: arrayOf(number),
  summary: string
}

DailyForecast.defaultProps = {
  date: new Date().toString(),
  weather: '',
  temperature: [0, 0],
  wind: [0, 0],
  summary: ''
}

export default DailyForecast
