
import React from 'react'
import { observer } from 'mobx-react'
import css from './Overall.css'
import icon from './Icon.css'

const Overall = ({ overall: data }) => (
  <div className={css.container}>
    <div className={css.content}>
      <div className={icon('weather', 'cloudy', css.main_icon)} />
      <div>
        {data.weather}
        <div className={css.secondary}>
          <div className={icon.temperature} />
          <span className={css.secondary_content}>{data.temperature}°C</span>
          <div className={icon.humidity} />
          <span className={css.secondary_content}>{data.humidity}%</span>
        </div>
      </div>
    </div>
  </div>
)

const { shape, string, number } = React.PropTypes

Overall.propTypes = {
  overall: shape({
    weather: string,
    temperature: number,
    humidity: number
  })
}

Overall.defaultProps = {
  overall: { weather: 'Fair', temperature: 0, humidity: 0 }
}

export default observer(['overall'], Overall)
