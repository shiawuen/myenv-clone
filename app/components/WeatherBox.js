
import React from 'react'
import css from './WeatherBox.css'
import icon from './Icon.css'

const WeatherBox = (props) => (
  <div className={props.className}>
    <WeatherBoxItem location='north' weather={props.north} />
    <WeatherBoxItem location='west' weather={props.west} />
    <WeatherBoxItem location='south' weather={props.south} />
    <WeatherBoxItem location='east' weather={props.east} />
    <WeatherBoxItem location='center' weather={props.center} />
  </div>
)

const { string } = React.PropTypes

WeatherBox.propTypes = {
  north: string,
  west: string,
  south: string,
  east: string,
  center: string
}

export default WeatherBox

export const WeatherBoxItem = ({location, status}) => (
  <div className={icon('weather', status, css[location])} />
)

WeatherBoxItem.propTypes = {
  location: string,
  status: string
}

WeatherBoxItem.defaultProps = {
  location: 'center',
  status: 'fair'
}
