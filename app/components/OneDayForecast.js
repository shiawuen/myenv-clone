
import React from 'react'
import { observer } from 'mobx-react'
import css from './OneDayForecast.css'
import icon from './Icon.css'
import WeatherBox from './WeatherBox.js'
import RadioButtons from './RadioButtons.js'

const Summary = ({ data: { key, label, value, measure } }) => (
  <div className={css.summary_item}>
    <header>
      <span className={icon(key, css.icon)} />
      {label}
    </header>
    {value[0]}-{value[1]}{measure}
  </div>
)

export default observer(['one_day_forecast'], ({one_day_forecast: data}) => (
  <section className={css.wrapper}>
    <header className={css.header}>
      <h1>24-Hour Forecast</h1>
    </header>

    <article className={css.summaries}>
      {data.summary.map(item => (
        <Summary key={item.key} data={item} />
      ))}
    </article>

    <WeatherBox
      {...data.periods.morning}
      className={css.weather_box}
    />

    <RadioButtons
      name='periods'
      className={css.radio_buttons}
      items={Object.keys(data.periods)}
      selected={Object.keys(data.periods).shift()}
    />
  </section>
))

