
import React from 'react'
import { observer } from 'mobx-react'
import css from './Widgets.css'
import icon from './Icon.css'

const { shape, arrayOf, string, number } = React.PropTypes

const render = (data) => {
  switch (data.type) {
    case 'psi': return <WidgetPSI key={data.type} {...data} />
    case 'dengue': return <WidgetDengue key={data.type} {...data} />
    default: return <Widget key={data.type} {...data} />
  }
}

export const Widget = (data) => (
  <article className={css.item}>
    <header>{data.label}</header>
    <div className={css.content}>{data.value}</div>
    <footer>{data.measure}</footer>
  </article>
)

Widget.propTypes = {
  type: string,
  label: string,
  value: number,
  measure: string
}

Widget.defaultProps = {
  type: '',
  label: '',
  value: 0,
  measure: ''
}

export const WidgetAdd = () => (
  <article className={css('item', 'add')}>
    <span className={css('icon', icon.add)} />
    Add
  </article>
)

export const WidgetDengue = (data) => (
  <article className={css.item}>
    <header>{data.label}</header>
    <div className={css.content}>
      <div className={css('dengue', data.measure.toLowerCase())} />
    </div>
  </article>
)

WidgetDengue.propTypes = Widget.propTypes
WidgetDengue.defaultProps = Widget.defaultProps

export const WidgetPSI = (data) => (
  <article className={css.item}>
    <header>{data.label}</header>
    <div className={css.content}>
      <div className={css('psi', data.measure.toLowerCase())}>
        {data.value}
      </div>
    </div>
    <footer>{data.measure}</footer>
  </article>
)

WidgetPSI.propTypes = Widget.propTypes
WidgetPSI.defaultProps = Widget.defaultProps

const Widgets = ({widgets: data}) => (
  <div className={css.wrapper}>
    <div className={css.container}>
      {data.map(item => render(item))}
      <WidgetAdd />
    </div>
  </div>
)

Widgets.propTypes = {
  widgets: arrayOf(shape({
    key: string,
    label: string,
    value: number,
    measure: string
  }))
}

Widgets.defaultProps = {
  widgets: []
}

export default observer(['widgets'], Widgets)
