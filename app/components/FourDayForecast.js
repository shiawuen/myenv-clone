
import React from 'react'
import { observer } from 'mobx-react'
import css from './FourDayForecast.css'
import DailyForecast from './DailyForecast.js'

export default observer(['four_day_forecast'], ({ four_day_forecast: data }) => (
  <section className={css.wrapper}>
    <h1>4-day Outlook</h1>

    {data.map(d => <DailyForecast key={d.date} {...d} />)}
  </section>
))
