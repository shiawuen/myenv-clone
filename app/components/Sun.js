
import React from 'react'

export default ({ x = 0, y = 0 }) => (
  <g classname='sun' fill='none' fill-rule='evenodd' transform={`translate(${x}, ${y})`}>
    <ellipse fill='#f90' cx='14.333' cy='14.667' rx='8.333' ry='8.333' />
    <rect fill='#f90' transform='rotate(-42 23 6)' x='21.333' y='5.333' width='3.333' height='1.333' rx='.667' />
    <rect fill='#f90' x='25.351' y='13.944' width='3.333' height='1.333' rx='.667' />
    <rect fill='#f90' transform='rotate(90 14.35 1.944)' x='12.685' y='1.277' width='3.333' height='1.333' rx='.667' />
    <rect fill='#f90' transform='rotate(42 5.685 5.944)' x='4.018' y='5.277' width='3.333' height='1.333' rx='.667' />
    <rect fill='#f90' transform='rotate(-42 5.35 23.61)' x='3.685' y='22.944' width='3.333' height='1.333' rx='.667' />
    <rect fill='#f90' transform='rotate(90 14.35 27.277)' x='12.685' y='26.611' width='3.333' height='1.333' rx='.667' />
    <rect fill='#f90' transform='rotate(42 23.35 23.61)' x='21.685' y='22.944' width='3.333' height='1.333' rx='.667' />
    <rect fill='#f90' y='14' width='3.333' height='1.333' rx='.667' />
    <path d='m16.61 8.432c-.223 0-.51.19-.593.6.424.204 1.634 1.363 2.266 2.087.7.8 1.546 2.06 1.728 2.487.35.16.774-.184.774-.53 0-.466-.996-1.764-1.67-2.515-.718-.797-2.147-2.13-2.505-2.13z' fill='#fffefd' />
  </g>
)
