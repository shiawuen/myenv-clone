
import React, { PropTypes } from 'react'
import css from './RadioButtons.css'

const RadioButtons = ({ name, items, selected, className }) => (
  <div className={css('wrapper', className)}>
    {items.map(item => (
      <RadioButton key={item}
        name={name}
        value={item}
        selected={selected}
      />)
    )}
    <div className={css.decoration} />
  </div>
)

const { array, string } = PropTypes

RadioButtons.propTypes = {
  name: string.isRequired,
  items: array,
  selected: string,
  className: string
}

RadioButtons.defaultProps = {
  items: [],
  className: ''
}

export default RadioButtons

export const RadioButton = ({ name, value, selected }) => (
  <label>
    <input type='radio'
      name={name}
      className={css.input}
      defaultChecked={selected === value}
    />
    <span className={css.item}>
      {value}
    </span>
  </label>
)

