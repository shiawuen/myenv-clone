
import React from 'react'
import { findDOMNode } from 'react-dom'
import { observer } from 'mobx-react'
import { format as formatDate } from 'fecha'
import css from './TideNSunForecast.css'
import icon from './Icon.css'
import Sun from './Sun.js'

const fmtTime = (date) => formatDate(date, 'h:mm a')

const CurrentDate = ({ date }) => {
  const day = formatDate(date, 'Do').split(/st|nd|rd|th/)
  return (
    <div className={css.current_date}>
      {day[0]}
      <sup>{day[1]}</sup>
      {' '}
      {formatDate(date, 'MMMM YYYY')}
    </div>
  )
}

const CurrentMarker = observer(['tide_n_sun'], ({ tide_n_sun: d }) => (
  <g transform={`translate(${d.x(d.currentTime)}, 0)`}>
    <rect
      x='0' y={d.heightScale(0.2)}
      width='1' height={d.heightScale(0.8)}
      fill='url(#white-to-gray)'
    />
    <polygon className={css.current_marker_triangle}
      points={`0,${d.heightScale(1) - 8} 8,${d.heightScale(1)} -8,${d.heightScale(1)}`}
    />
  </g>
))

const Tide = observer(['tide_n_sun'], ({ tide_n_sun: d }) => (
  <g>
    <path d={d.tides.line} className={css.tide_line} />
    <path d={d.tides.area} className={css.tide_area} />
    <line className={css.bottom_line}
      x1='0' y1={d.visualizationHeight}
      x2={d.svgWidth} y2={d.visualizationHeight}
    />
  </g>
))

class SunPath extends React.Component {
  constructor () {
    super()
    this.state = { show: false, coords: { x: 0, y: 0 } }
  }
  componentDidMount () {
    this.getSunCoords()
  }
  componentWillReceiveProps () {
    this.getSunCoords()
  }
  getSunCoords () {
    const { diff } = this.props
    if (diff === -1) return void this.setState({ show: false })
    const node = findDOMNode(this.refs.path)
    const length = node.getTotalLength()
    const { x, y } = node.getPointAtLength(diff * length)
    this.setState({ show: true, coords: { x, y } })
  }
  renderSun () {
    if (!this.state.show) return
    const { x, y } = this.state.coords
    return (<Sun x={x - 14} y={y - 14} />)
  }
  render () {
    return (
      <g>
        <path ref='path' d={this.props.path} className={css.sun_path} />
        {this.renderSun()}
      </g>
    )
  }
}

const Suns = observer(['tide_n_sun'], ({ tide_n_sun: d }) => (
  <g>
    {d.suns.map((s, i) => (
      <g key={s.start.value.toString()}>
        <SunPath
          diff={d.suns_diff[i]}
          path={s.path}
        />
        <text className={css.sun_text}
          x={s.start.coords[0]}
          y={s.start.coords[1]}
          transform='translate(0, 19)'
          children={fmtTime(s.start.value)}
        />
        <text className={css.sun_text}
          x={s.end.coords[0]}
          y={s.end.coords[1]}
          transform='translate(0, 19)'
          children={fmtTime(s.end.value)}
        />
      </g>
    ))}
  </g>
))

const Nights = observer(['tide_n_sun'], ({ tide_n_sun: d }) => (
  <g>
    {d.nights.map(night => (
      <rect {...night} className={css.night} />
    ))}
  </g>
))

const Labels = observer(['tide_n_sun'], ({ tide_n_sun: d }) => (
  <g>
    {d.tides.items.filter(d => !d.hide).map(tide => (
      <g key={tide.date} className={css.label} transform={`translate(${tide.coords.join(',')})`}>
        <rect className={css.label_box}
          width='60' height='35'
          x='-30' y='-30'
          rx='5' ry='5'
        />
        <text className={css.label_tide}
          x='0' y='-1.2em'
          children={`${tide.height}m`}
        />
        <text className={css.label_time}>{fmtTime(tide.date)}</text>
      </g>
    ))}
  </g>
))

@observer(['tide_n_sun'])
class ScrollWrapper extends React.Component {
  updatePosition () {
    const left = findDOMNode(this).scrollLeft
    this.props.tide_n_sun.updateScroll(left)
  }
  componentDidMount () {
    this.updatePosition()
  }
  onScroll (e) {
    this.updatePosition()
  }
  render () {
    return (<div
      className={css.visualization}
      children={this.props.children}
      onScroll={this.onScroll.bind(this)}
    />)
  }
}

const TideNSunForecast = ({
  tide_n_sun: d
}) => (
  <section className={css.wrapper}>
    <header>
      <h1 className={css.header}>
        Tide •
        {' '}
        <span className={css.suns}>
          Sunrise & Sunset
        </span>
      </h1>
    </header>
    <ScrollWrapper>
      <svg height={d.dimension.height} width={d.svgWidth}>
        <defs>
          <linearGradient id='white-to-gray' x1='0%' y1='0%' x2='0%' y2='100%'>
            <stop offset='20%' stopColor='#ffffff' />
            <stop offset='100%' stopColor='#cecece' />
          </linearGradient>
        </defs>
        <g>
          <Tide />
          <Suns />
          <Labels />
          <Nights />
          <CurrentMarker />
        </g>
      </svg>
    </ScrollWrapper>
    <div
      className={icon('moon', d.moon, css('moon', { is_night: d.is_night }))}
      style={{ top: d.heightScale(0.35) }}
    />
    <CurrentDate date={d.currentTime} />
    <div className={css.current_time}>
      <span>
        {fmtTime(d.currentTime)}
      </span>
    </div>
  </section>
)

export default observer(['tide_n_sun'], TideNSunForecast)
