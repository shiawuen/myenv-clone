
import React from 'react'
import { Provider } from 'mobx-react'
import { render as renderReact } from 'react-dom'
import tide_n_sun from './tide-n-sun.js' // eslint-disable-line camelcase

const states = {
  tide_n_sun,
  overall: {
    weather: 'Cloudy',
    temperature: 27.9,
    humidity: 92
  },
  widgets: [
    { type: 'psi',
      label: 'PSI',
      value: 31,
      measure: 'Good'
    },
    { type: 'rain',
      label: 'rain',
      value: 0,
      measure: 'mm'
    },
    { type: 'dengue',
      label: 'Dengue',
      value: 0,
      measure: 'Good'
    }
  ],
  one_day_forecast: {
    summary: [
      { key: 'temperature', label: 'Temp', value: [25, 34], measure: '°C' },
      { key: 'humidity', label: 'Humidity', value: [60, 95], measure: '%' },
      { key: 'wind', label: 'Wind', value: [15, 35], measure: 'km/h' }
    ],
    periods: {
      afternoon: { north: 'fair', west: 'fair', south: 'fair', east: 'fair', center: 'fair' },
      night: { north: 'cloudy', west: 'cloudy', south: 'cloudy', east: 'cloudy', center: 'cloudy' },
      morning: { north: 'rain', west: 'thunder_showers', south: 'thunder_showers', east: 'thunder_showers', center: 'thunder_showers' }
    }
  },
  four_day_forecast: [
    { date: '2016-09-24',
      weather: 'thunder_showers',
      temperature: [24, 33],
      wind: [15, 30],
      summary: 'Predawn hours and early morning thundery showers.'
    },
    { date: '2016-09-25',
      weather: 'fair',
      temperature: [24, 33],
      wind: [10, 15],
      summary: 'Predawn hours and early morning thundery showers.'
    },
    { date: '2016-09-26',
      weather: 'rain',
      temperature: [25, 34],
      wind: [10, 30],
      summary: 'Late morning and early afternoon thundery showers.'
    },
    { date: '2016-09-27',
      weather: 'cloudy',
      temperature: [25, 34],
      wind: [10, 20],
      summary: 'Late morning and early afternoon thundery showers.'
    }
  ]
}

const render = (App) => {
  renderReact(
    <Provider {...states}>
      <App />
    </Provider>,
    document.getElementById('app')
  )
}

let App = require('./App').default
if (module.hot) {
  module.hot.accept('./App.js', () => {
    let App = require('./App.js').default
    render(App)
  })
}
render(App)

