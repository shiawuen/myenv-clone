
export const HOUR = 60 * 60 * 1000
export const DAY = 24 * HOUR

export const hoursBefore = (date, hours = 0) => {
  return new Date(+new Date(date) - hours * HOUR)
}

export const daysDiff = (range) => {
  return (+range[1] - +range[0]) / DAY
}
