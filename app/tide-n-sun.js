
import assign from 'object-assign'
import {
  parse as parseDate,
  format as formatDate
} from 'fecha'
import { observable, action } from 'mobx'
import { HOUR, daysDiff, hoursBefore } from './date-helper.js'
import { area, line, curveCardinal } from 'd3-shape'
import { scaleLinear } from 'd3-scale'
import _data from './tide_sun.json'

const HEIGHT_RATIO = 0.75
const DESKTOP_HEIGHT_RATIO = 0.5
const TIDE_HEIGHT_RATIO = 1
const MAX_TIDE_HEIGHT = 7
const PEAK_SUN = '13:00:00'
const BOTTOM_LEGEND_HEIGHT = '30'

const getHeightRatio = () => {
  return window.matchMedia('all and (min-width: 800px)').matches
    ? DESKTOP_HEIGHT_RATIO
    : HEIGHT_RATIO
}

const toDate = window.TODATE = (dateStr) => {
  return parseDate(dateStr, 'YYYY-MM-DD HH:mm:ss')
}

const computeDimension = () => {
  const width = document.documentElement.clientWidth
  return {
    width, height: width * getHeightRatio()
  }
}

const data = observable({
  now: new Date(2016, 8, 23, 15, 42, 0),
  currentTime: new Date(2016, 8, 23, 15, 42, 0),
  data: _data,
  dimension: computeDimension(),
  get daysDiff () {
    return daysDiff([this.startDate, this.endDate])
  },
  get startDate () {
    return hoursBefore(this.now, 6)
  },
  get endDate () {
    const date = this.data[this.data.length - 1].date
    return toDate(`${date} 23:59:59`)
  },
  get moons () {
    return this.data
      .map(d => [d.date, d.moon])
      .reduce((acc, d) => assign(acc, { [d[0]]: d[1] }), {})
  },
  get moon () {
    const date = formatDate(this.currentTime, 'YYYY-MM-DD')
    return this.moons[date]
  },
  get suns () {
    return this.data.map(d => {
      const start = toDate(`${d.date} ${d.sun.rise}`)
      const end = toDate(`${d.date} ${d.sun.set}`)
      const peak = toDate(`${d.date} ${PEAK_SUN}`)

      const l = line()
        .x(d => this.x(d[0]))
        .y(d => this.y(d[1]))
        .curve(curveCardinal.tension(-0.5))

      const path = l([[start, 0], [peak, 3], [end, 0]])

      return {
        path,
        start: { coords: [this.x(start), this.y(0)], value: start },
        end: { coords: [this.x(end), this.y(0)], value: end }
      }
    })
  },
  get suns_diff () {
    return this.suns.map(({ start: { value: start }, end: { value: end } }) => {
      if (start > this.currentTime || this.currentTime > end) return -1
      const max = end - start
      const now = this.currentTime - start
      return now / max
    })
  },
  get is_night () {
    for (let i = 0; i < this._nights.length; i++) {
      let night = this._nights[i]
      let inRange = night[0] < this.currentTime && this.currentTime < night[1]
      if (inRange) return true
    }
    return false
  },
  get _nights () {
    return this.data.map(d => [
      [toDate(`${d.date} 00:00:00`), toDate(`${d.date} 05:55:59`)],
      [toDate(`${d.date} 20:00:00`), toDate(`${d.date} 23:59:59`)]
    ])
    .reduce((acc, d) => acc.concat(d), [])
  },
  get nights () {
    const y = this.heightScale
    return this._nights
      .map(([a, b]) => ({
        key: a + b,
        x: this.x(a),
        y: y(0),
        width: this.x(b) - this.x(a),
        height: y(1)
      }))
  },
  get _tides () {
    const tides = this.data.map(d =>
      d.tides.map(t => ({
        date: toDate(`${d.date} ${t.time}`),
        dstr: `${d.date} ${t.time}`,
        height: t.height
      }))
    ).reduce((acc, d) => acc.concat(d), [])
    if (this.now - tides[0].date < 6 * HOUR) {
      tides.unshift(assign({}, tides[0], {
        date: this.startDate,
        hide: true
      }))
    }
    const last = tides[tides.length - 1]
    tides.push(assign({}, last, {
      date: this.endDate,
      hide: true
    }))
    return tides
  },
  get tides () {
    const a = area()
      .x(d => this.x(d.date))
      .y0(this.visualizationHeight)
      .y1(d => this.y(d.height))
      .curve(curveCardinal.tension(-0.25))

    const l = line()
      .x(d => this.x(d.date))
      .y(d => this.y(d.height))
      .curve(curveCardinal.tension(-0.25))

    return {
      area: a(this._tides),
      line: l(this._tides),
      items: this._tides.map(tide => assign({}, tide, {
        coords: [this.x(tide.date), this.y(tide.height)]
      }))
    }
  },
  get x () {
    return scaleLinear()
      .domain([this.startDate, this.endDate])
      .range([0, this.svgWidth])
  },
  get y () {
    return scaleLinear()
      .range([0, this.visualizationHeight])
      .domain([MAX_TIDE_HEIGHT, 0])
  },
  get heightScale () {
    return scaleLinear()
      .domain([0, 1])
      .range([0, this.visualizationHeight])
  },
  get scroll2date () {
    const end = new Date(this.endDate)
    end.setHours(18)
    end.setMinutes(0)
    end.setSeconds(0)
    return scaleLinear()
      .range([this.now, end])
      .domain([0, this.svgWidth - this.dimension.width])
  },
  get svgHeight () {
    return this.dimension.height * TIDE_HEIGHT_RATIO
  },
  get svgWidth () {
    return this.daysDiff * this.dimension.width * 2
  },
  get visualizationHeight () {
    return this.svgHeight - BOTTOM_LEGEND_HEIGHT
  },
  updateScroll: action(function (left) {
    this.currentTime = this.scroll2date(left)
  })
})

global.addEventListener('resize', () => {
  data.dimension = computeDimension()
})
global.addEventListener('orientationchange', () => {
  data.dimension = computeDimension()
})

export default data
