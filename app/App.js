
import React from 'react'
import Overall from './components/Overall.js'
import Widgets from './components/Widgets.js'
import OneDayForecast from './components/OneDayForecast.js'
import FourDayForecast from './components/FourDayForecast.js'
import TideNSunForecast from './components/TideNSunForecast.js'
import './App.css'

export default () => (
  <div>
    <Overall />
    <Widgets />
    <TideNSunForecast />
    <OneDayForecast />
    <FourDayForecast />
  </div>
)
