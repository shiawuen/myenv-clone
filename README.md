
## myENV clone

### Prerequisites

- Node.js
- Git

### Installation

```bash
git clone git@gitlab.com:shiawuen/myenv-clone.git
cd myenv-clone
npm i
```

### Run the development server

Run the command below and visit http://localhost:8080

```bash
npm run start
```

### Running the tests

```bash
npm run test # for single run
npm run test:watch # for continuous minitor changes
```

### Create the build files for deployment

Run the command below, you should have the compiled files created under `PROJECT_ROOT/dist` folder.

```bash
npm run build
```

### Tides • Sunrise & Sunset visualization

The visualization rely on:

1. `d3-scale`: To compute the tides & suns curves, coordinates for suns & tides labels, and width/height of the chart
1. `d3-shape`: To generate the curves with help from scales produce with `d3-scale`

The visualization is render with an array of dataset similar as below, where each day will have a set of data.

Reference `app/tide_sun.json`

```json
{ "date": "2016-09-23",
  "sun": { "rise": "06:54:00", "set": "19:01:00" },
  "tides": [
    { "time": "10:33:00", "height": 1.2 },
    { "time": "15:26:00", "height": 2.7 },
    { "time": "22:22:00", "height": 0.5 }
  ],
  "moon": "last-quarter"
}
```

The visualization is render as wide SVG, it is then wrapped with a scroll wrapper to limit its visible area, the `scrollLeft` property of it is use to set the current time cursor.

The rendering breaks into parts below:

1. Scaling of the SVG
1. Rendering for Tides
1. Rendering for Nights
1. Rendering for Suns and Moons

#### Scaling of the SVG

1. Get the screen dimension
1. Compute days difference between `startDate` and `endDate`
  1. `startDate` = 6 hours before `now`
  1. `endDate` = 23:59:59 of the last date in the dataset
1. Compute the SVG width with `daysDiff * width * 2`, 2 times because of 12-hour display
1. Render the SVG with the calculated with and height

**NOTE**:
I use a fixed `now` because there is only 1 dataset with fixed datetime.

#### Rendering for Tides

Using `d3-scale` and `d3-shape` for help.

1. Create the scales of `x` and `y` to plot the tides
  1. `x` with `domain = startDate to endDate`, `range = 0 to SVG width`
  1. `y` with `domain = MAX_TIDE_HEIGHT to 0`, `range = 0 to SVG height`
1. Use `line` and `area` of `d3-shape` to render the waves
1. Plot the tides label with the scales created above

#### Rendering for Nights

We assume the sky is dark between

1. `00:00:00 — 05:59:59`
1. `20:00:00 — 23:59:59`

With the time frames above, renders a darker rectangles on the SVG during the time frames above using the `x` scale earlier

#### Rendering for Suns and Moons

Since we only know the sunrise and sunset time,
we assume that 13:00:00 is the peak of sun to allow the arc plotting

1. Use `line` and the scales created earlier to plot the arc with sunrise, peak, and sunset time.
1. To plot the sun, we use the current time to get the percentage of how much it is into the range of current date’s sunrise and sunset time frame.
1. With the value we then use  `SVGPathElement`’s `getTotalLength` and `getPointAtLength` to get the coordinates of the supposed x and y at current time and put it at exact location
1. With time frame on rendering nights, we assume moon only shows up during those time frame, then it is easy as always putting it at exact location at the center and only show it when current time in the night time frames.
